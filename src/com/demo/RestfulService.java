package com.demo;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

@Path("/personas")
public class RestfulService {

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Persona> listar() {
		List<Persona> lista = new ArrayList<Persona>();
		lista.add(new Persona(1, "Juan", "Perez"));
		lista.add(new Persona(2, "Miguel", "Lopez"));
		return lista;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{id}")
	public Persona getData(@PathParam("id") String id) {
		if ("1".equals(id)) {
			return new Persona(1, "Persona", "Uno");
		}
		if ("2".equals(id)) {
			return new Persona(2, "Persona", "Dos");
		}
		throw new WebApplicationException(404);
	}

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public void save(Persona data) {
		System.out.println(data.toString() + "creado");
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Persona update(@PathParam("id") int id, Persona data) {
		data.setId(id);
		if (id == 1 || id == 2) {
			System.out.println(data.toString() + " actualizado");
		} else {
			System.out.println(data.toString() + " creado");
		}
		return data;
	}

	@DELETE
	@Path("{id}")
	public void remove(@PathParam("id") int id) {
		if (id == 1 || id == 2) {
			System.out.println("data removed");
		} else {
			throw new WebApplicationException(404);
		}
	}
}
